# FPGA on USB Port

<table style="padding:10px">
    <tr>
        <td><img src="./screenshots/front.png" alt="1" width=100% height=auto></td>
        <td><img src="./screenshots/back.png" alt="1" width=100% height=auto></td>
        <td><img src="./screenshots/orthogonal.png" alt="1" width=100% height=auto></td>
    </tr>
    <tr>
        <td>Front View</td>
        <td>Back View</td>
        <td>Orthogonal View</td>
    </tr>
</table>

## Features & Specifications

PCB fits entirely inside a standard USB Type-A slot, except for a small area that pokes out to give you access to four copper pads. There is one RGB LED that lights up the case and is fully user-controllable. The main chip is an FPGA with about 5000 LUTs, enough for a CPU with some room left over. The FPGA has 1024 kilo-bits of memory available. A separate block of memory is used for things like the processor register file, in addition to temporary memory for things like USB buffers. The CPU can use 64 or 128 kilobytes of memory, depending on configuration. It has four copper pads near the edge.

- **FPGA:** Lattice ICE40UP5K
- **Speed:** 48 MHz external oscillator
- **RAM:** 128 kB RAM for a soft CPU
- **Storage:** 1 MB SPI flash
- **Connectivity:** USB 2.0 FS (12 Mbps)
- **Buttons:** Four
- **LEDs:** One RGB

## Micro Python, RISC-V, FPGA

With 128 kilobytes of RAM and a large amount of storage, PCB can able run Python natively. And since it lives in your USB port, installation is super simple. FPGAs are complicated, but the latest Python tools make it easy to use without any specialized training.

Underneath the Python interpreter lies a RISC-V softcore running on the FPGA fabric. RISC-V is an up-and-coming processor architecture that is poised to take over everything from deeply-embedded chips to high-performance computing.

An FPGA is a piece of reconfigurable silicon. The firmware exposes a USB bootloader running a RISC-V softcore, but we can load whatever we want. Softcores are also available for LM32 and OpenRISC.
## Firmware

Please follow the instructions on https://github.com/timvideos/litex-buildenv/wiki/HowTo-FuPy-on-iCE40-Boards to install RISC-V Soft Core and Micro Python over the PCB. 